FROM php:7.4.3-apache

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    libzip-dev \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl

# Install extensions
RUN apt-get update -y
RUN docker-php-ext-install gd
RUN apt-get install libsodium-dev -y
RUN docker-php-ext-install mysqli pdo pdo_mysql
#RUN docker-php-ext-install sodium
#RUN docker-php-ext-install mysqli
RUN docker-php-ext-install zip   

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN a2enmod rewrite

RUN curl -sS https://getcomposer.org/installer | php && \
 mv composer.phar /usr/local/bin/composer
 
COPY composer.json /var/www/html

COPY . /var/www/html

WORKDIR /var/www/html

RUN composer install
