<?php

namespace App\Controller;

use PDO;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(title="Creditas API", version="0.1")
 */

class LeadController
{
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @OA\Get(
     *     path="/leads",
     *     @OA\Response(response="201", description="Success"),
     *     @OA\Response(
     *         response=404,
     *         description="Failure"
     *     ) 
     * )
    */

    public function getAll(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface
    {
        $sql = "SELECT * FROM leads;";
        $statement = $this->connection->prepare($sql);
        $statement->execute();

        $row = $statement->fetchAll();

        $response->getBody()->write(json_encode($row));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(201);
    }

    
    /**
     * @OA\Get(
     *     path="/lead/{id}",
     *     @OA\Parameter(
     *        name="id",
     *        in="path",
     *        required=true,
     *        description="ID of the lead",
     *        @OA\Schema(
     *            type="integer"
     *        )
     *     ),
     *     @OA\Response(response="201", description="Success"),
     *     @OA\Response(
     *         response=404,
     *         description="Failure"
     *     ) 
     * )
    */
    public function get(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface
    {
        try {
            $id = (int)$args['id'];
            $sql = "SELECT * FROM leads WHERE id = :id;";
            $statement = $this->connection->prepare($sql);
            $statement->execute(['id' => $id]);

            $row = $statement->fetch();

            if (empty($row)) {
                $response->getBody()->write(json_encode(['message' => 'No se pudo obtener la informacion.']));

                return $response
                    ->withHeader('Content-Type', 'application/json')
                    ->withStatus(404);
            }

            if ($row['lead_tipo'] == 'AUTO') {
                $sql_lead = 'SELECT * FROM auto_leads';
            }

            if ($row['lead_tipo'] == 'NOMINA') {
                $sql_lead = 'SELECT * FROM auto_leads';
            }

            if ($row['lead_tipo'] == 'CASA') {
                $sql_lead = 'SELECT * FROM auto_leads';
            }

            $statement_leads = $this->connection->prepare($sql_lead);
            $statement_leads->execute(['lead_id' => $row['id']]);

            $row_lead = $statement_leads->fetch();
            $row['lead'] = $row_lead;

            $response->getBody()->write(json_encode($row));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        }
        catch (PDOException $e) {
            $response->getBody()->write(json_encode(['message' => 'No se pudo obtener la informacion.']));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(404);
        }
    }

    
    /**
     * @OA\Post(
     *     path="/lead/auto/create",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="nombre",
     *                     description="Nombre del cliente",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="telefono",
     *                     description="Telefono del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="rfc",
     *                     description="RFC del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="calle",
     *                     description="Calle del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="numero_exterior",
     *                     description="Numero exterior del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="colonia",
     *                     description="Colonia del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="ciudad",
     *                     description="Ciudad del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="estado",
     *                     description="Estado del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="monto",
     *                     description="Monto o valor.",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="auto_modelo",
     *                     description="Modelo del auto.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="auto_marca",
     *                     description="Marca del auto.",
     *                     type="string"
     *                 ),
     *                 required={"nombre","email","telefono","rfc","calle","numero_exterior","colonia","ciudad","estado","monto","auto_modelo","auto_marca"},
     *             )
     *         )
     *     ),
     *     @OA\Response(response="201", description="Success"),
     *     @OA\Response(
     *         response=404,
     *         description="Failure"
     *     ) 
     * )
    */
    public function createLeadAuto(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $row = [
            'nombre' => $data['nombre'],
            'email' => $data['email'],
            'telefono' => $data['telefono'],
            'rfc' => $data['rfc'],
            'calle' => $data['calle'],
            'numero_exterior' => $data['numero_exterior'],
            'colonia' => $data['colonia'],
            'ciudad' => $data['ciudad'],
            'estado' => $data['estado'],
            'monto' => $data['monto'],
            'auto_modelo' => $data['auto_modelo'],
            'auto_marca' => $data['auto_marca']
        ];

        try {
            $row['cliente_id'] = self::createCliente($row);

            $row['status'] = ($data['monto'] < 200000 || $data['monto'] > 500000) ? 'RECHAZADO' : 'ACEPTADO';

            $row['lead_tipo'] = 'AUTO';
            $row['lead_id'] = self::createLead($row);

            $sql = "INSERT INTO auto_leads SET 
                    lead_id = '" . $row['lead_id'] . "', 
                    auto_modelo = '" . $row['auto_modelo'] . "',
                    auto_marca = '" . $row['auto_marca'] . "',
                    created = '" . date("Y-m-d H:i:s") . "';";

            $this->connection->prepare($sql)->execute($row);
            $last_id = (int)$this->connection->lastInsertId();

            $result = [
                'lead_id' => $row['lead_id'],
                'lead_tipo' => $row['lead_tipo'],
                'status' => $row['status']
            ];

            $response->getBody()->write(json_encode($result));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        }
        catch (PDOException $e) {
            $response->getBody()->write(json_encode(['message' => 'No se pudo realizar la operación.']));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(404);
        }

        
    }


    /**
     * @OA\Post(
     *     path="/lead/nomina/create",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="nombre",
     *                     description="Nombre del cliente",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="telefono",
     *                     description="Telefono del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="rfc",
     *                     description="RFC del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="calle",
     *                     description="Calle del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="numero_exterior",
     *                     description="Numero exterior del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="colonia",
     *                     description="Colonia del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="ciudad",
     *                     description="Ciudad del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="estado",
     *                     description="Estado del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="monto",
     *                     description="Monto o valor.",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="empresa",
     *                     description="Empresa donde labora el cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="fecha_ingreso",
     *                     description="Fecha de ingreso del cliente.",
     *                     type="string"
     *                 ),
     *                 required={"nombre","email","telefono","rfc","calle","numero_exterior","colonia","ciudad","estado","monto","empresa","fecha_ingreso"},
     *             )
     *         )
     *     ),
     *     @OA\Response(response="201", description="Success"),
     *     @OA\Response(
     *         response=404,
     *         description="Failure"
     *     ) 
     * )
    */
    public function createLeadNomina(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $row = [
            'nombre' => $data['nombre'],
            'email' => $data['email'],
            'telefono' => $data['telefono'],
            'rfc' => $data['rfc'],
            'calle' => $data['calle'],
            'numero_exterior' => $data['numero_exterior'],
            'colonia' => $data['colonia'],
            'ciudad' => $data['ciudad'],
            'estado' => $data['estado'],
            'monto' => $data['monto'],
            'empresa' => $data['empresa'],
            'fecha_ingreso' => $data['fecha_ingreso']
        ];

        try {
            $row['cliente_id'] = self::createCliente($row);

            $row['status'] = (strtotime($data['fecha_ingreso']) > strtotime('-14months')) ? 'RECHAZADO' : 'ACEPTADO';

            $row['lead_tipo'] = 'NOMINA';
            $row['lead_id'] = self::createLead($row);

            $sql = "INSERT INTO nomina_leads SET 
                    lead_id = '" . $row['lead_id'] . "', 
                    empresa = '" . $row['empresa'] . "',
                    fecha_ingreso = '" . $row['fecha_ingreso'] . "',
                    created = '" . date("Y-m-d H:i:s") . "';";

            $this->connection->prepare($sql)->execute($row);
            $last_id = (int)$this->connection->lastInsertId();

            $result = [
                'lead_id' => $row['lead_id'],
                'lead_tipo' => $row['lead_tipo'],
                'status' => $row['status']
            ];

            $response->getBody()->write(json_encode($result));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        }
        catch (PDOException $e) {
            $response->getBody()->write(json_encode(['message' => 'No se pudo obtener la informacion.']));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(404);
        }
    }


    /**
     * @OA\Post(
     *     path="/lead/auto/create",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="nombre",
     *                     description="Nombre del cliente",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="telefono",
     *                     description="Telefono del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="rfc",
     *                     description="RFC del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="calle",
     *                     description="Calle del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="numero_exterior",
     *                     description="Numero exterior del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="colonia",
     *                     description="Colonia del domicilio.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="ciudad",
     *                     description="Ciudad del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="estado",
     *                     description="Estado del cliente.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="monto",
     *                     description="Monto o valor.",
     *                     type="number"
     *                 ),
     *                 @OA\Property(
     *                     property="calle_inmueble",
     *                     description="Calle del inmueble.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="numero_exterior_inmueble",
     *                     description="Numero exterior del inmueble.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="colonia_inmueble",
     *                     description="Colonia del inmueble.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="ciudad_inmueble",
     *                     description="Ciudad del inmueble.",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="estado_inmueble",
     *                     description="Estado del inmueble.",
     *                     type="string"
     *                 ),
     *                 required={"nombre","email","telefono","rfc","calle","numero_exterior","colonia","ciudad","estado","monto","calle_inmueble","numero_exterior_inmueble","colonia_inmueble","ciudad_inmueble","estado_inmueble"},
     *             )
     *         )
     *     ),
     *     @OA\Response(response="201", description="Success"),
     *     @OA\Response(
     *         response=404,
     *         description="Failure"
     *     ) 
     * )
    */
    public function createLeadCasa(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface
    {
        $data = (array)$request->getParsedBody();

        $row = [
            'nombre' => $data['nombre'],
            'email' => $data['email'],
            'telefono' => $data['telefono'],
            'rfc' => $data['rfc'],
            'calle' => $data['calle'],
            'numero_exterior' => $data['numero_exterior'],
            'colonia' => $data['colonia'],
            'ciudad' => $data['ciudad'],
            'estado' => $data['estado'],
            'monto' => $data['monto'],
            'calle_inmueble' => $data['calle_inmueble'],
            'numero_exterior_inmueble' => $data['numero_exterior_inmueble'],
            'colonia_inmueble' => $data['colonia_inmueble'],
            'ciudad_inmueble' => $data['ciudad_inmueble'],
            'estado_inmueble' => $data['estado_inmueble'],
        ];

        try {
            $row['cliente_id'] = self::createCliente($row);

            $row['status'] = ($data['estado_inmueble'] == 'Estado de México' || $data['estado_inmueble'] == 'Ciudad de México') ? 'ACEPTADO' : 'RECHAZADO';

            $row['lead_tipo'] = 'CASA';
            $row['lead_id'] = self::createLead($row);

            $sql = "INSERT INTO casa_leads SET 
                    lead_id = '" . $row['lead_id'] . "', 
                    calle = '" . $row['calle_inmueble'] . "',
                    numero_exterior = '" . $row['numero_exterior_inmueble'] . "',
                    colonia = '" . $row['colonia_inmueble'] . "',
                    ciudad = '" . $row['ciudad_inmueble'] . "',
                    estado = '" . $row['estado_inmueble'] . "',
                    created = '" . date("Y-m-d H:i:s") . "';";

            $this->connection->prepare($sql)->execute($row);
            $last_id = (int)$this->connection->lastInsertId();

            $result = [
                'lead_id' => $row['lead_id'],
                'lead_tipo' => $row['lead_tipo'],
                'status' => $row['status']
            ];

            $response->getBody()->write(json_encode($result));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(201);
        }
        catch (PDOException $e) {
            $response->getBody()->write(json_encode(['message' => 'No se pudo obtener la informacion.']));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(404);
        }
    }

    public function createCliente($row)
    {
        $sql = "INSERT INTO clientes SET 
                nombre = '" . $row['nombre'] . "', 
                email = '" . $row['email'] . "',
                telefono = '" . $row['telefono'] . "',
                rfc = '" . $row['rfc'] . "',
                calle = '" . $row['calle'] . "',
                numero_exterior = '" . $row['numero_exterior'] . "',
                colonia = '" . $row['colonia'] . "',
                ciudad = '" . $row['ciudad'] . "',
                estado = '" . $row['estado'] . "',
                created = '" . date("Y-m-d H:i:s") . "';";

        $this->connection->prepare($sql)->execute();

        return (int)$this->connection->lastInsertId();
    }


    public function createLead($row)
    {
        $sql = "INSERT INTO leads SET 
                cliente_id = " . $row['cliente_id'] .", 
                lead_tipo = '" . $row['lead_tipo'] . "',
                lead_monto = '" . $row['monto'] . "',
                status = '" . $row['status'] . "',
                created = '" . date("Y-m-d H:i:s") . "';";

        $this->connection->prepare($sql)->execute();

        return (int)$this->connection->lastInsertId();
    }

}