CREATE DATABASE IF NOT EXISTS test;
USE test;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Clientes
-- ----------------------------
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL UNIQUE,
  `telefono` varchar(128),
  `rfc` varchar(128),
  `calle` varchar(128),
  `numero_exterior` varchar(128),
  `colonia` varchar(128),
  `ciudad` varchar(128),
  `estado` varchar(128),
  `created` timestamp NOT NULL,
  `updated` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `clientes` (`nombre`, `email`, `telefono`, `rfc`, `calle`, `numero_exterior`, `colonia`, `ciudad`, `estado`, `created`) VALUES ('Juan Martínez', 'juanmartin@gmail.com', '4771234567', 'MTJU900917GTY7H', 'Adolfo Ruiz', '324', 'Presidentes', 'Ciudad de México', 'Ciudad de México', '2021-11-01 01:58:41');


-- ----------------------------
-- Leads
-- ----------------------------
DROP TABLE IF EXISTS `leads`;
CREATE TABLE IF NOT EXISTS `leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) NOT NULL,
  `lead_tipo` varchar(115) NOT NULL,
  `lead_monto` decimal(15,2) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'NUEVO',
  `created` timestamp NOT NULL,
  `updated` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `leads` (`cliente_id`, `lead_tipo`, `lead_monto`, `status`, `created`) VALUES (1, 'AUTO', '300000', 'ACEPTADO', '2020-11-09 01:58:41');


-- ----------------------------
-- Auto_Leads
-- ----------------------------
DROP TABLE IF EXISTS `auto_leads`;
CREATE TABLE IF NOT EXISTS `auto_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_id` int(11) NOT NULL,
  `auto_modelo` varchar(100) NOT NULL,
  `auto_marca` varchar(100) NOT NULL,
  `created` timestamp NOT NULL,
  `updated` timestamp NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `auto_leads_fk` FOREIGN KEY (`lead_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auto_leads` (`lead_id`, `auto_modelo`, `auto_marca`, `created`) VALUES (1, '2019', 'Ford', '2020-11-09 01:58:41');


-- ----------------------------
-- Nomina_Leads
-- ----------------------------
DROP TABLE IF EXISTS `nomina_leads`;
CREATE TABLE IF NOT EXISTS `nomina_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_id` int(11) NOT NULL,
  `empresa` varchar(100) NOT NULL,
  `fecha_ingreso` timestamp NOT NULL,
  `created` timestamp NOT NULL,
  `updated` timestamp NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `nomina_leads_fk` FOREIGN KEY (`lead_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ----------------------------
-- Casa_Leads
-- ----------------------------
DROP TABLE IF EXISTS `casa_leads`;
CREATE TABLE IF NOT EXISTS `casa_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_id` int(11) NOT NULL,
  `calle` varchar(100) NOT NULL,
  `numero_exterior` varchar(128),
  `colonia` varchar(128),
  `ciudad` varchar(128),
  `estado` varchar(128),
  `created` timestamp NOT NULL,
  `updated` timestamp NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `casa_leads_fk` FOREIGN KEY (`lead_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
