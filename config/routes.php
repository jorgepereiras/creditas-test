<?php

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;

return function (App $app) {
    $app->get('/', function (
        ServerRequestInterface $request,
        ResponseInterface $response
    ) {
        $response->getBody()->write('Hello Creditas!');

        return $response;
    });

    $app->get('/leads', [\App\Controller\LeadController::class, 'getAll']);
    $app->get('/lead/{id}', [\App\Controller\LeadController::class, 'get']);
    $app->post('/lead/auto/create', [\App\Controller\LeadController::class, 'createLeadAuto']);
    $app->post('/lead/nomina/create', [\App\Controller\LeadController::class, 'createLeadNomina']);
    $app->post('/lead/casa/create', [\App\Controller\LeadController::class, 'createLeadCasa']);
};
