# Guía de instalación

## Clonar repositorio
Clone el repositorio `creditas-api` en su directorio:
```sh
git clone git@gitlab.com:jorgepereiras/creditas-test.git
```

## Composer
Sitúese en el directorio del proyecto e instale las dependencias de `composer`;
```sh
cd creditas-test
composer install
```

## Docker
Para correr el contenedor, ejecute los siguientes comandos:
```sh
docker-compose build
docker-compose up -d
```
Después ingrese a `http://localhost:8080`

## Base de datos
Para revisar la base de datos mediante `PHPMyAdmin` puede ingresar a `http://localhost:5000`

## Documentación de API
Ingrese a `http://localhost:8080/doc`